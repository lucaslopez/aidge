Aidge Export TensorRT API
=========================


Export function
---------------

.. autofunction:: aidge_export_tensorrt.export

Plugin helper
-------------

The TensorRT export allow you to define plugins which will be automatically used when loadig the ONNX file.

The export define an helper command to generate the template of a plugin which you can then fill up.


**Usage example:**

.. code-block::

    python -m aidge_export_tensorrt.generate_plugin -n "test" -f "myExport"

This will create the plugin ``test`` in the folder ``myExport``

..  code-block::

    myExport
    +--- plugins
    |   +--- test
    |   |   +--- test_plugin.hpp
    |   |   +--- test_plugin.cu
    *--- ...
