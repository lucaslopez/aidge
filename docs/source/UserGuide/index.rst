User Guide
==========

🚧 Please be aware that this user guide is still an incomplete and only partially implemented specification of the Aidge framework. Please refer to the :ref:`API section <source/API/index:API>` to get the current state of Aidge's API.


Workflow overview
-----------------

AIDGE allows designing and deploying Deep Neural Networks (DNN) on embedded systems.
The design and deployment stages are as follows:

.. image:: /source/_static/AidgeWorkflow.PNG

High level functionalities
--------------------------

AIDGE offers functionalities that can be categorized according to the following diagram.

-	:ref:`Load and store model <source/userguide/loadStoreModel:Load and store model>` functions used to load or store a graph model from/to a serialized format.
-	:ref:`Model graph <source/userguide/modelGraph:Syntax to create a computational graph>`: functions used to model a graph such as adding an operator.
-	:ref:`Transform model <source/userguide/transformGraph:Graph transformation>`: functions used for manipulating the graph, such as graph duplication.
-	:ref:`Provide data<source/userguide/data:Data Structure>`: functions used for providing data in order to execute a graph on data. These functions must be runnable on device.
-	:ref:`Generate graph <source/userguide/executeGraph:Implementations and backends>`: generate kernels and scheduling for a specific target, on an already optimized graph. There is no graph manipulation here, the graph is supposed to be already prepared (quantization, tiling, operator mapping…) for the intended target. For graph preparation and optimization, see :ref:`Optimize graph<source/userguide/optimizeGraph:Optimize graph>`.
-	:ref:`Static analysis of the graph <source/userguide/staticAnalysis:Static analysis>`: functions for obtaining statics on the graph like number of parameters, operations etc, that can be computed without having to execute the graph.
-	:ref:`Execute graph <source/userguide/executeGraph:Runtime graph execution>`: execute a graph, either using a backend library (simple implementation change, no generation or compilation involved), or compiled :ref:`operators <source/userguide/modelGraph:Operator>` implementation, after passing through the Compile graph function in this case.
-	:ref:`Learn model <source/userguide/learnModel:Learn model>`: performing a training requires several functions of the workflow (Model graph, Provide data, Execute graph, Benchmark KPI).
-	:ref:`Benchmark KPI<source/userguide/benchmark:Benchmark neural network model>`: all kind of benchmarking requiring to run the network on a target in order to perform a measurement: accuracy, execution time… Some of these functions must be runable on device.
-	:ref:`Model Hardware<source/userguide/export:Model hardware & architecture rules>`: functions to represent the hardware target.
-	:ref:`Optimize graph<source/userguide/optimizeGraph:Optimize graph>`: high-level functions to optimize the hardware mapping: quantization, pruning, tiling…
-	:ref:`Learn on edge<source/userguide/learnEdge:Learn on edge>`: high-level functions to condition the graph for edge learning, including continual learning and federated learning.
-	:ref:`Ensure robustness<source/userguide/ensureRobustness:Ensure robustness>`: high-level functions to condition the graph for robustness.

.. toctree::
    :hidden:
    :maxdepth: 2

    architecture.rst
    data.rst
    modelGraph.rst
    loadStoreModel.rst
    interoperability.rst
    transformGraph.rst
    staticAnalysis.rst
    executeGraph.rst
    learnModel.rst
    benchmark.rst
    ensureRobustness.rst
    optimizeGraph.rst
    export.rst
    learnEdge.rst
