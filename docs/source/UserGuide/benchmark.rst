Benchmark neural network model
==============================

Overview
--------

The platform must be able to evaluate the performance of the trained models. This can be done by calculating an application score, which can depend on the task solved by the model or by other measures such as the execution time layer by layer.
Some of these functions must be executable on the hardware target!

Compute score metrics
---------------------

For each of the following task, AIDGE give access to the following metrics: 

**Classification**

* Compute precision/recall
* Compute confusion matrix

**Segmentation**

* Pixel accuracy
* Compute IoU

**Detection**

* Compute IoU

Measure excution time
---------------------

AIDGE can provide the execution time :ref:`operator <source/userguide/modelGraph:Operator>` wise.