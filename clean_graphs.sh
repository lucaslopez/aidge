#!/bin/bash

# Get the directory in which the script is located
script_directory="$(pwd)"

# Use find to locate files with the .mmd extension and print the list
files_to_remove=$(find "$script_directory" -type f -name "*.mmd")
echo "$files_to_remove"

find "$script_directory" -type f -name "*.mmd" -exec rm -f {} +

echo "Files with .mmd extension removed in $(pwd)"
